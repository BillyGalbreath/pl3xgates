package net.pl3x.bukkit.gates.event;

import net.pl3x.bukkit.gates.gate.Channel;
import org.bukkit.entity.Player;

/**
 * Called when a player is attempting to create a new channel
 */
public class CreateChannelEvent extends Pl3xGatesEvent {
    private Channel channel;

    /**
     * Create new channel event
     *
     * @param player  Player creating the channel
     * @param channel The channel being created
     */
    public CreateChannelEvent(Player player, Channel channel) {
        super(player);

        this.channel = channel;
    }

    /**
     * Get the channel being created
     *
     * @return Created channel
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Set the channel being created
     *
     * @param channel Created channel
     */
    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
