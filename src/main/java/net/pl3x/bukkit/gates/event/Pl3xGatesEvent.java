package net.pl3x.bukkit.gates.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Base event for all Pl3xGates' events to extend
 */
public abstract class Pl3xGatesEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancel;

    public Pl3xGatesEvent(Player player) {
        super(player);
    }

    /**
     * Check if event has been cancelled
     *
     * @return True if cancelled
     */
    @Override
    public boolean isCancelled() {
        return cancel;
    }

    /**
     * Set cancelled state of event
     *
     * @param cancel True to cancel
     */
    @Override
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @SuppressWarnings("unused")
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
