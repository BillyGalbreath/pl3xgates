package net.pl3x.bukkit.gates;

import net.pl3x.bukkit.gates.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Chat {
    private String message;

    /**
     * Create chat message from Lang entry
     *
     * @param lang Lang entry
     */
    public Chat(Lang lang) {
        this(lang.toString());
    }

    /**
     * Create chat message from String
     *
     * @param message The chat message
     */
    public Chat(String message) {
        this.message = message;

        colorize(); // auto color new chat messages
    }

    /**
     * Send the chat message to CommandSender
     *
     * @param recipient CommandSender that receives this chat message
     */
    public void send(CommandSender recipient) {
        if (message == null || ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }

    /**
     * Broadcast the chat message to console and all online players
     */
    public void broadcast() {
        Bukkit.getOnlinePlayers().forEach(this::send);
        send(Bukkit.getConsoleSender()); // always include console in broadcasts
    }

    /**
     * Convert & color codes to minecraft color codes
     */
    private void colorize() {
        message = ChatColor.translateAlternateColorCodes('&', message);
    }
}
