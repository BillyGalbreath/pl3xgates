package net.pl3x.bukkit.gates;

import net.pl3x.bukkit.gates.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Class to handle logging output to console
 */
public class Logger {
    /**
     * Output to console
     *
     * @param msg Message to output
     */
    private static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + Pl3xGates.getPlugin(Pl3xGates.class).getName() + "&3]&r " + msg);
        if (!Config.COLOR_LOGS.getBoolean()) {
            msg = ChatColor.stripColor(msg);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(msg);
    }

    /**
     * Log a debug message
     *
     * @param msg Debug message
     */
    public static void debug(String msg) {
        if (Config.DEBUG_MODE.getBoolean()) {
            Logger.log("&7[&eDEBUG&7]&e " + msg);
        }
    }

    /**
     * Log a warning message
     *
     * @param msg Warning message
     */
    public static void warn(String msg) {
        Logger.log("&e[&6WARN&e]&6 " + msg);
    }

    /**
     * Log an error message
     *
     * @param msg Error message
     */
    public static void error(String msg) {
        Logger.log("&e[&4ERROR&e]&4 " + msg);
    }

    /**
     * Log an info message
     *
     * @param msg Info message
     */
    public static void info(String msg) {
        Logger.log("&e[&fINFO&e]&r " + msg);
    }
}
