package net.pl3x.bukkit.gates;

import net.pl3x.bukkit.gates.command.CmdPl3xGates;
import net.pl3x.bukkit.gates.configuration.Lang;
import net.pl3x.bukkit.gates.listener.CreationListener;
import net.pl3x.bukkit.gates.manager.GateManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xGates extends JavaPlugin {
    private GateManager gateManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        getServer().getPluginManager().registerEvents(new CreationListener(this), this);

        getCommand("pl3xgates").setExecutor(new CmdPl3xGates(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    /**
     * Get Pl3xGates instance
     *
     * @return Pl3xGates instance
     */
    public static Pl3xGates getPlugin() {
        return Pl3xGates.getPlugin(Pl3xGates.class);
    }

    /**
     * Get the gate manager
     *
     * @return Gate manager
     */
    public GateManager getGateManager() {
        if (gateManager == null) {
            gateManager = new GateManager();
        }
        return gateManager;
    }
}
