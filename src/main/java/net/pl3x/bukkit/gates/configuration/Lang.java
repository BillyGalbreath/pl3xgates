package net.pl3x.bukkit.gates.configuration;

import java.io.File;
import net.pl3x.bukkit.gates.Logger;
import net.pl3x.bukkit.gates.Pl3xGates;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Language entries for regional/customized message support
 */
public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),
    PLAYER_COMMAND("&4Player only command!"),

    VERSION("&d{plugin} v{version}"),
    RELOAD("&d{plugin} v{version} reloaded.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    /**
     * Reload lang file if not already loaded into memory
     */
    public static void reload() {
        reload(false);
    }

    /**
     * Reload lang file
     *
     * @param force True to force memory overwrite from disk
     */
    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xGates.getPlugin().getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xGates.getPlugin().saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    /**
     * Get the key for this entry
     *
     * @return Entry key
     */
    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get String entry from memory
     *
     * @return String entry
     */
    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    /**
     * Replace String with a String
     *
     * @param find    String to find
     * @param replace String to take its place
     * @return String result
     */
    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}