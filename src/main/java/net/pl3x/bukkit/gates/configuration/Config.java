package net.pl3x.bukkit.gates.configuration;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import net.pl3x.bukkit.gates.Pl3xGates;
import org.bukkit.Material;

/**
 * The main config.yml manager
 */
public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),

    GATE_SIGN_TAG("gate"),

    MAXIMUM_FRAME_SIZE(20),
    MINIMUM_FRAME_SIZE(10),

    FRAME_MATERIALS(Arrays.asList("WOOD", "OBSIDIAN", "IRON_BLOCK"));

    private final Pl3xGates plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xGates.getPlugin();
        this.def = def;
    }

    /**
     * Reload config from disk to memory
     */
    public static void reload() {
        Pl3xGates.getPlugin().reloadConfig();
    }

    /**
     * Get entry key
     *
     * @return Entry key
     */
    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get string value
     *
     * @return String value
     */
    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    /**
     * Get boolean value
     *
     * @return Boolean value
     */
    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    /**
     * Get int value
     *
     * @return int value
     */
    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    /**
     * Get materials listed in config
     *
     * @return Collection of materials
     */
    public Collection<Material> getMaterials() {
        Collection<Material> materials = new HashSet<>();
        for (String materialName : plugin.getConfig().getStringList(getKey())) {
            try {
                materials.add(Material.valueOf(materialName.toUpperCase()));
            } catch (IllegalArgumentException ignore) {
            }
        }
        return materials;
    }
}
