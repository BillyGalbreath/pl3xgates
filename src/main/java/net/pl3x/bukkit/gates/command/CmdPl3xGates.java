package net.pl3x.bukkit.gates.command;

import java.util.ArrayList;
import java.util.List;
import net.pl3x.bukkit.gates.Chat;
import net.pl3x.bukkit.gates.Pl3xGates;
import net.pl3x.bukkit.gates.configuration.Config;
import net.pl3x.bukkit.gates.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class CmdPl3xGates implements TabExecutor {
    private final Pl3xGates plugin;

    public CmdPl3xGates(Pl3xGates plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> result = new ArrayList<>();
        if (args.length == 1 && "reload".startsWith(args[0].toLowerCase())) {
            result.add("reload");
        }
        return result;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0) {
            if ("reload".equalsIgnoreCase(args[0])) {
                if (!sender.hasPermission("command.pl3xgates.reload")) {
                    new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
                    return true;
                }

                Config.reload();
                Lang.reload(true);

                new Chat(Lang.RELOAD
                        .replace("{plugin}", plugin.getName())
                        .replace("{version}", plugin.getDescription().getVersion()))
                        .send(sender);
                return true;
            }

            if ("info".equalsIgnoreCase(args[0])) {
                if (!sender.hasPermission("command.pl3xgates.info")) {
                    new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
                    return true;
                }

                if (!(sender instanceof Player)) {
                    new Chat(Lang.PLAYER_COMMAND).send(sender);
                    return true;
                }

                //
                return true;
            }
        }

        if (!sender.hasPermission("command.pl3xgates")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        new Chat(Lang.VERSION
                .replace("{version}", plugin.getDescription().getVersion())
                .replace("{plugin}", plugin.getName()))
                .send(sender);

        return true;
    }
}