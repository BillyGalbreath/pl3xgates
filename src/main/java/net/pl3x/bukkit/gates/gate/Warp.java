package net.pl3x.bukkit.gates.gate;

import java.util.Collection;
import java.util.HashSet;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.MaterialData;

/**
 * Represents a gate's warp area
 */
public class Warp {
    private final Collection<Location> warps = new HashSet<>();

    /**
     * Create new warp area
     *
     * @param warps Warp locations in area
     */
    public Warp(Collection<Location> warps) {
        this.warps.addAll(warps);
    }

    /**
     * Get warp locations in area
     *
     * @return Warp locations
     */
    public Collection<Location> getLocations() {
        return warps;
    }

    /**
     * Set warp locations to a filler type
     *
     * @param filler Filler type
     */
    public void setFiller(MaterialData filler) {
        for (Location point : getLocations()) {
            Block block = point.getBlock();
            block.setType(filler.getItemType(), false);
            block.setData(filler.getData(), false);
        }
    }

    /**
     * Set warp locations to air then forget all locations
     */
    public void clear() {
        setFiller(new MaterialData(Material.AIR, (byte) 0));
        warps.clear();
    }
}
