package net.pl3x.bukkit.gates.gate;

import jdk.nashorn.internal.ir.Block;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.material.MaterialData;

/**
 * Represents a warp gate
 */
public class Gate {
    private final String name;
    private final Channel channel;
    private final Location sign;
    private final MaterialData filler;
    private final Frame frame;
    private final Warp warp;

    private Block button;
    private String destination;
    private int destinationMarker = -1;

    /**
     * Create new Gate object
     *
     * @param name    Name of gate
     * @param channel Channel gate is on
     * @param sign    Sign that controls gate destination
     * @param filler  Warp area filler material
     * @param frame   Gate frame blocks
     * @param warp    Gate warp area
     */
    public Gate(String name, Channel channel, Location sign, MaterialData filler, Frame frame, Warp warp) {
        this.name = name;
        this.channel = channel;
        this.sign = sign;
        this.filler = filler;
        this.frame = frame;
        this.warp = warp;
    }

    /**
     * Get name of gate
     *
     * @return Name of gate
     */
    public String getName() {
        return name;
    }

    /**
     * Get gate's channel
     *
     * @return Channel of gate
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Get the destination sign
     *
     * @return Destination sign
     */
    public Location getSign() {
        return sign;
    }

    /**
     * Get filler material
     *
     * @return Filler material
     */
    public MaterialData getFiller() {
        return filler;
    }

    /**
     * Get the gate's frame
     *
     * @return Frame of gate
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * Get the gate's warp area
     *
     * @return Warp area
     */
    public Warp getWarp() {
        return warp;
    }

    /**
     * Get the gate's activation button
     *
     * @return Activation button
     */
    public Block getButton() {
        return button;
    }

    /**
     * Set the gate's activation button
     *
     * @param button Activation button
     */
    public void setButton(Block button) {
        this.button = button;
    }

    /**
     * Get the name of the destination gate
     *
     * @return Destination gate's name
     */
    public String getDestination() {
        return destination;
    }

    /**
     * Set the name of the destination gate
     *
     * @param destination Destination gate's name
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * Get the destination marker position on the gate's sign
     *
     * @return Marker position
     */
    public int getDestinationMarker() {
        return destinationMarker;
    }

    /**
     * Set the destination marker position on the gate's sign
     *
     * @param destinationMarker Marker position
     */
    public void setDestinationMarker(int destinationMarker) {
        this.destinationMarker = destinationMarker;
    }

    /**
     * Deactivate gate (changes warp area to air)
     */
    public void deactivate() {
        getWarp().setFiller(new MaterialData(Material.AIR, (byte) 0));
    }

    /**
     * Activate gate (changes warp area to filler)
     */
    public void activate() {
        getWarp().setFiller(filler);
    }

    /**
     * Deactivate gate and clear as much data as possible
     */
    public void unload() {
        deactivate();

        setButton(null);
        setDestination(null);
        setDestinationMarker(-1);

        getWarp().clear();
        getFrame().clear();
    }
}
