package net.pl3x.bukkit.gates.gate;

import java.util.Collection;
import java.util.HashSet;

/**
 * Represents a gate channel
 */
public class Channel {
    private final String name;
    private final Collection<Gate> gates = new HashSet<>();

    /**
     * Create a new gate channel
     *
     * @param name Name of channel
     */
    public Channel(String name) {
        this.name = name;
    }

    /**
     * Get the channel name
     *
     * @return Channel name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the gates on this channel
     *
     * @return Gates on channel
     */
    public Collection<Gate> getGates() {
        return gates;
    }

    /**
     * Add gate to channel
     *
     * @param gate Gate to add
     */
    public void addGate(Gate gate) {
        gates.add(gate);
    }

    /**
     * Remove gate from channel
     *
     * @param gate Gate to remove
     */
    public void removeGate(Gate gate) {
        gate.unload();
        gates.remove(gate);
    }

    /**
     * Remove all gates from channel
     */
    public void removeAllGates() {
        gates.forEach(Gate::unload);
        gates.clear();
    }
}
