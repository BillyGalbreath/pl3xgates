package net.pl3x.bukkit.gates.gate;

import java.util.Collection;
import java.util.HashSet;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * Represents a gate's frame
 */
public class Frame {
    private final Collection<Block> blocks = new HashSet<>();

    /**
     * Create new frame of blocks
     *
     * @param blocks Frame blocks
     */
    public Frame(Collection<Block> blocks) {
        this.blocks.addAll(blocks);
    }

    /**
     * Get blocks that make up this frame
     *
     * @return Frame blocks
     */
    public Collection<Block> getBlocks() {
        return blocks;
    }

    /**
     * Check if block is part of the frame
     *
     * @param block Block to check
     * @return True if block is part of frame
     */
    public boolean isFrameBlock(Block block) {
        return blocks.contains(block);
    }

    /**
     * Check if this location is part of the frame
     *
     * @param location Location to check
     * @return True if part of frame
     */
    public boolean isFrameBlock(Location location) {
        return blocks.contains(location.getBlock());
    }

    /**
     * Forget all blocks
     */
    public void clear() {
        blocks.clear();
    }
}
