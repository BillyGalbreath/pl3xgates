package net.pl3x.bukkit.gates.listener;

import java.util.HashSet;
import java.util.Set;
import net.pl3x.bukkit.gates.Pl3xGates;
import net.pl3x.bukkit.gates.configuration.Config;
import net.pl3x.bukkit.gates.event.CreateChannelEvent;
import net.pl3x.bukkit.gates.gate.Channel;
import net.pl3x.bukkit.gates.manager.GateManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class CreationListener implements Listener {
    private Pl3xGates plugin;

    public CreationListener(Pl3xGates plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onGateSignCreation(SignChangeEvent event) {
        // check if block is still a sign
        Block block = event.getBlock();
        if (!(block.getState() instanceof org.bukkit.block.Sign)) {
            return; // not a sign block anymore. no action/ignore
        }

        // check if sign is a wall sign
        if (!block.getType().equals(Material.WALL_SIGN)) {
            return; // not a wall sign. no action/ignore
        }

        // check if gate already exists for this sign
        Player player = event.getPlayer();
        org.bukkit.block.Sign signBlock = (org.bukkit.block.Sign) block.getState();
        GateManager gateManager = plugin.getGateManager();
        if (gateManager.getGate(signBlock) != null) {
            // notify player
            event.setCancelled(true);
            return; // do not edit existing gate signs. cancel event only
        }

        // check sign for gate tag
        String[] lines = event.getLines();
        if (!lines[0].equalsIgnoreCase("[" + Config.GATE_SIGN_TAG.getString() + "]")) {
            return; // not creating a gate. ignore
        }

        // check if player has permission to create gates
        if (!player.hasPermission("pl3xgates.create.gates")) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // no permission to make gate. cancel event and break sign.
        }

        // check name of gate on sign
        String gateName = lines[1];
        if (gateName.isEmpty()) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // missing gate name. cancel event and break sign.
        }

        // check name length (need room for destination marker)
        if (gateName.length() > 11) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // gate name too long. cancel event and break sign.
        }

        // check channel on sign
        String channelName = lines[2];
        if (channelName.isEmpty()) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // missing gate channel. cancel event and break sign.
        }

        // check if channel exists
        Channel channel = gateManager.getChannel(channelName);
        if (channel != null) {
            // check if gate name is already in use on this channel
            if (gateManager.getGate(gateName, channelName) != null) {
                // notify player
                event.setCancelled(true);
                block.breakNaturally();
                return; // gate name already in use on this channel. cancel event and break sign.
            }
        } else {
            // check if can create new channels
            if (!player.hasPermission("pl3xgates.create.channels")) {
                // notify player
                event.setCancelled(true);
                block.breakNaturally();
                return; // no permission to make new channel. cancel event and break sign.
            }

            // call create channel event
            CreateChannelEvent createChannelEvent = new CreateChannelEvent(player, new Channel(channelName.toLowerCase()));
            Bukkit.getPluginManager().callEvent(createChannelEvent);
            if (createChannelEvent.isCancelled()) {
                // do not notify player. cancelling plugin will handle notification
                event.setCancelled(true);
                block.breakNaturally();
                return; // other plugin has cancelled channel creation. cancel event and break sign.
            }

            // create channel, but do not add to manager yet
            channel = createChannelEvent.getChannel();
        }

        // check if frame material is allowed (sign placed on frame)
        org.bukkit.material.Sign signMat = (org.bukkit.material.Sign) signBlock.getData();
        Block frameStart = block.getRelative(signMat.getAttachedFace());
        Material frameMaterial = frameStart.getType();
        if (!Config.FRAME_MATERIALS.getMaterials().contains(frameMaterial)) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // frame not correct material. cancel event and break sign.
        }

        // check if frame is connected loop (clockwise check)
        int maxFrameSize = Config.MAXIMUM_FRAME_SIZE.getInt();
        int minFrameSize = Config.MINIMUM_FRAME_SIZE.getInt();
        BlockFace startFace = signMat.getFacing();
        switch (startFace) {
            case NORTH:
                startFace = BlockFace.WEST;
                break;
            case WEST:
                startFace = BlockFace.SOUTH;
                break;
            case SOUTH:
                startFace = BlockFace.EAST;
                break;
            case EAST:
                startFace = BlockFace.NORTH;
        }
        BlockFace currentFace = BlockFace.UP;
        BlockFace nextFace = startFace;
        Set<Block> frameBlocks = new HashSet<>();
        boolean complete = false;
        while (!complete && frameBlocks.size() < maxFrameSize) {
            Block testBlock = block.getRelative(currentFace);
            Block testBlock2 = testBlock.getRelative(nextFace);
            if (testBlock2.getType().equals(frameMaterial)) {
                if (frameBlocks.contains(testBlock2)) {
                    complete = true;
                    continue;
                }
                frameBlocks.add(testBlock2);
                block = testBlock2;
                currentFace = rotateFace(currentFace, startFace);
                nextFace = rotateFace(nextFace, startFace);
                continue;
            }
            testBlock2 = testBlock;
            if (testBlock2.getType().equals(frameMaterial)) {
                if (frameBlocks.contains(testBlock2)) {
                    complete = true;
                    continue;
                }
                frameBlocks.add(testBlock2);
                block = testBlock2;
                continue;
            }
            testBlock2 = testBlock.getRelative(nextFace.getOppositeFace());
            if (testBlock2.getType().equals(frameMaterial)) {
                if (frameBlocks.contains(testBlock2)) {
                    complete = true;
                    continue;
                }
                frameBlocks.add(testBlock2);
                block = testBlock2;
            }
        }

        // check frame too big
        if (!complete || frameBlocks.size() > maxFrameSize) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // frame too big. cancel event and break sign.
        }

        // check frame too small
        if (frameBlocks.size() < minFrameSize) {
            // notify player
            event.setCancelled(true);
            block.breakNaturally();
            return; // frame too small. cancel event and break sign.
        }

        // check warp area size


        // check filler material and data
        //

        // call create gate event
        //

        // create gate object
        //

        // throw creation event
        //
        // check cancelled

        // add channel to manager
        gateManager.addChannel(channel);

        // add sign to manager
        //

        // save gate and channel to disk
        //

        // animate/activate gate
        //
    }

    private BlockFace rotateFace(BlockFace face, BlockFace right) {
        if (face.equals(BlockFace.UP)) {
            return right;
        }
        if (face.equals(BlockFace.DOWN)) {
            return right.getOppositeFace();
        }
        if (face.equals(right)) {
            return BlockFace.DOWN;
        }
        return BlockFace.UP;
    }
}
