package net.pl3x.bukkit.gates.manager;

import java.util.Collection;
import java.util.HashSet;
import net.pl3x.bukkit.gates.gate.Channel;
import net.pl3x.bukkit.gates.gate.Gate;
import org.bukkit.Location;
import org.bukkit.block.Sign;

/**
 * The gate manager
 */
public class GateManager {
    private final Collection<Channel> channels = new HashSet<>();

    /**
     * Get channel by name
     *
     * @param name Name of channel to get
     * @return Channel with name, or null
     */
    public Channel getChannel(String name) {
        for (Channel channel : channels) {
            if (channel.getName().equalsIgnoreCase(name)) {
                return channel;
            }
        }
        return null;
    }

    /**
     * Add a new channel
     *
     * @param channel Channel to add
     */
    public void addChannel(Channel channel) {
        channels.add(channel);
    }

    /**
     * Get gate by sign
     *
     * @param sign Controller sign
     * @return Gate, or null if not found
     */
    public Gate getGate(Sign sign) {
        Location singLoc = sign.getLocation();
        for (Channel channel : channels) {
            for (Gate gate : channel.getGates()) {
                if (gate.getSign().equals(singLoc)) {
                    return gate;
                }
            }
        }
        return null;
    }

    /**
     * Get gate by name and channel
     *
     * @param channelName Name of channel
     * @param gateName    Name of gate
     * @return Gate with name on channel, or null if not found
     */
    public Gate getGate(String channelName, String gateName) {
        Channel channel = getChannel(channelName);
        if (channel == null) {
            return null;
        }
        for (Gate gate : channel.getGates()) {
            if (gate.getName().equals(gateName)) {
                return gate;
            }
        }
        return null;
    }

    /**
     * Deactivate and clear all gates and channels
     */
    public void clear() {
        channels.forEach(Channel::removeAllGates);
        channels.clear();
    }
}
